# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Material Design Datepicker Implementation

### Dependencies ###

* Angular Material, Material Design Lite, Moment, jQuery

**NOTE**: Make sure you install all the dependencies.

### Setup ###

1. npm i --save @angular-mdl/datepicker
2. You need to configure your system-config.js and add it in the map

   
```
#!angular2

'@angular-mdl/datepicker': 'node_modules/@angular-mdl/datepicker/index.umd.js',
```


3. In your index.html add this inside your head tag.
   
```
#!angular2

<link rel="stylesheet" href="node_modules/@angular-mdl/datepicker/datepicker.css">
```



### Usage ###

```
#!angular2

<ai-datepicker (evtSelectDate)="handleEvtSelectedDate($event)" 
               placeholder="From" >
</ai-datepicker>
```

How your systemjs.config.js > map will look like


```
#!angular2

      //base lib
      '@angular/material': 'npm:@angular/material/bundles/material.umd.js',
      'hammerjs': 'npm:hammerjs/hammer.js',

       // Material Design Lite 
       "material-design-lite": "node_modules/material-design-lite/material.min.js",

      '@angular/animations': 'npm:@angular/animations/bundles/animations.umd.js',
      '@angular/animations/browser': 'npm:@angular/animations/bundles/animations-browser.umd.js',
      '@angular/platform-browser/animations': 'npm:@angular/platform-browser/bundles/platform-browser-animations.umd.js',  

      //MDL datepicker
      '@angular-mdl/core': 'node_modules/@angular-mdl/core/bundle/core.js',
      '@angular-mdl/datepicker': 'node_modules/@angular-mdl/datepicker/index.umd.js',
```