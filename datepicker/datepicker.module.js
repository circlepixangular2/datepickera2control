/*******************************
    Created by: Reynel Fuertes
    Created date: April 2017
    Module Name: Datepicker
    Dependencies: Material Design(Base Lib), Material Design Lite(3rd party lib), Moment(3rd party Lib)
    Usage: <ai-datepicker (evtSelectDate)="YourParentFunction($event)" placeholder="To" ></ai-datepicker>
*******************************/
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var forms_1 = require("@angular/forms");
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var datepicker_component_1 = require("./datepicker.component");
var DTPickerModule = (function () {
    function DTPickerModule() {
    }
    return DTPickerModule;
}());
DTPickerModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, forms_1.FormsModule],
        exports: [datepicker_component_1.DatepickerComponent],
        declarations: [datepicker_component_1.DatepickerComponent],
        providers: [],
    })
], DTPickerModule);
exports.DTPickerModule = DTPickerModule;
//# sourceMappingURL=datepicker.module.js.map