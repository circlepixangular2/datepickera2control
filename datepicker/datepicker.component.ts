import { MdlDatePickerService } from '@angular-mdl/datepicker';
import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';

declare var $: any;
declare var moment: any;

@Component({
    moduleId: module.id,
    selector: 'ai-datepicker',
    templateUrl: 'datepicker.component.html',
    styleUrls: [ 'datepicker.component.css' ],
    encapsulation:  ViewEncapsulation.None,
})

export class DatepickerComponent implements OnInit, AfterViewInit {
    private guid: string;
    private selectedDate: any;
    @Input() placeholder: string;
    @Output() evtSelectDate: EventEmitter<string> = new EventEmitter<string>();

    constructor(private datePicker: MdlDatePickerService) {
    }

    ngOnInit() { 
        this.guid = "ABCD123";
    }

    public getSelectedDate(event: any) {
        this.datePicker.selectDate(this.selectedDate, {openFrom: event})
            .subscribe( (selectedDate: Date) => {
                this.selectedDate = selectedDate ? moment(selectedDate) : null;

                $('input[id="'+ this.guid +'"]').parents(2).addClass('is-dirty');

                this.evtSelectDate.emit(this.selectedDate);
            });
        
    }

    ngAfterViewInit() {
        $(document).ready(function() {
            
        }) 
    }
}