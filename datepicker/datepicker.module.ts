/*******************************
    Created by: Reynel Fuertes
    Created date: April 2017
    Module Name: Datepicker
    Dependencies: Material Design(Base Lib), Material Design Lite(3rd party lib), Moment(3rd party Lib)
    Usage: <ai-datepicker (evtSelectDate)="YourParentFunction($event)" placeholder="To" ></ai-datepicker>
*******************************/ 

import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { DatepickerComponent } from './datepicker.component';

@NgModule({
    imports: [ BrowserModule, FormsModule ],
    exports: [ DatepickerComponent ],
    declarations: [DatepickerComponent],
    providers: [],
})
export class DTPickerModule { }
