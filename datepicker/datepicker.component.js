"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var datepicker_1 = require("@angular-mdl/datepicker");
var core_1 = require("@angular/core");
var global_functions_1 = require("../../shared/global-functions");
var DatepickerComponent = (function () {
    function DatepickerComponent(datePicker) {
        this.datePicker = datePicker;
        this.evtSelectDate = new core_1.EventEmitter();
    }
    DatepickerComponent.prototype.ngOnInit = function () {
        this.guid = global_functions_1.makeid();
    };
    DatepickerComponent.prototype.getSelectedDate = function (event) {
        var _this = this;
        this.datePicker.selectDate(this.selectedDate, { openFrom: event })
            .subscribe(function (selectedDate) {
            _this.selectedDate = selectedDate ? moment(selectedDate) : null;
            $('input[id="' + _this.guid + '"]').parents(2).addClass('is-dirty');
            _this.evtSelectDate.emit(_this.selectedDate);
        });
    };
    DatepickerComponent.prototype.ngAfterViewInit = function () {
        $(document).ready(function () {
        });
    };
    return DatepickerComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], DatepickerComponent.prototype, "placeholder", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], DatepickerComponent.prototype, "evtSelectDate", void 0);
DatepickerComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'ai-datepicker',
        templateUrl: 'datepicker.component.html',
        styleUrls: ['datepicker.component.css'],
        encapsulation: core_1.ViewEncapsulation.None,
    }),
    __metadata("design:paramtypes", [datepicker_1.MdlDatePickerService])
], DatepickerComponent);
exports.DatepickerComponent = DatepickerComponent;
//# sourceMappingURL=datepicker.component.js.map